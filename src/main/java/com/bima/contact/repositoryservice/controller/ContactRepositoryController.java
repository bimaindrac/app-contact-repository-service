package com.bima.contact.repositoryservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bima.contact.common.dto.BaseListResponseDto;
import com.bima.contact.repositoryservice.dto.ContactDto;
import com.bima.contact.repositoryservice.service.ContactRepositoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api
@RestController
@RequestMapping("/api")
public class ContactRepositoryController {
	
	@Autowired
    private ContactRepositoryService contactRepositoryService;

	@ApiOperation("get contact list")
    @PostMapping(value = "/getcontactlist", produces = {MediaType.APPLICATION_JSON_VALUE})
    public BaseListResponseDto<ContactDto> getContactList() {
        return contactRepositoryService.getContactList();
    }
	
}
