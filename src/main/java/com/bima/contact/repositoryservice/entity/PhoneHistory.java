package com.bima.contact.repositoryservice.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "phone_history")
public class PhoneHistory implements Serializable {

	private static final long serialVersionUID = 6852528210479810075L;

	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "contact_id")
	private Long contactId;
	
	@Column(name = "phone_start")
    private Date phoneStart;
	
	@Column(name = "phone_end")
    private Date phoneEnd;
	
}
