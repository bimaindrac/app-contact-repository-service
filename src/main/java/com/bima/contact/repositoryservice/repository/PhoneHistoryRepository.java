package com.bima.contact.repositoryservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bima.contact.repositoryservice.entity.PhoneHistory;

@Repository
public interface PhoneHistoryRepository extends JpaRepository<PhoneHistory, Long> {
	
	public List<PhoneHistory> findByContactId(String contactId);
	
}
