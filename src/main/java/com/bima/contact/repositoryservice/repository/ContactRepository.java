package com.bima.contact.repositoryservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bima.contact.repositoryservice.entity.Contact;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {
	
}
