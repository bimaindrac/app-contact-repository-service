package com.bima.contact.repositoryservice.util;

import java.util.List;
import java.util.stream.Collectors;

import com.bima.contact.repositoryservice.dto.ContactDto;
import com.bima.contact.repositoryservice.entity.Contact;

public class ContactDtoUtil {
	
	private ContactDtoUtil() {
	}
	
	public static List<ContactDto> constructContactDtoList(List<Contact> list) {
		return list.stream().map(ContactDtoUtil::constructContactDtoList).collect(Collectors.toList());
	}

	public static ContactDto constructContactDtoList(Contact entity) {
		return ContactDto.builder()
				.id(entity.getId())
				.name(entity.getName())
				.phoneNum(entity.getPhoneNum())
				.address(entity.getAddress())
				.build();
	}
	
}
	
