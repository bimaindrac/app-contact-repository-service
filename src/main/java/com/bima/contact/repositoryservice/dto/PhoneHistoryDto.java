package com.bima.contact.repositoryservice.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PhoneHistoryDto implements Serializable {

	private static final long serialVersionUID = -6329458731151532800L;
	
	private Long id;
	private Long contactId;
    private Date phoneStart;
    private Date phoneEnd;
	
}
