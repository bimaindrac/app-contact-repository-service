package com.bima.contact.repositoryservice.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContactDto implements Serializable {

	private static final long serialVersionUID = 2489879627411713038L;
	
	private Long id;
	private String name;
	private String phoneNum;
	private String address;
	
}
