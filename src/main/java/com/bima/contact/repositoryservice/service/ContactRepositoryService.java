package com.bima.contact.repositoryservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bima.contact.common.constant.Constant;
import com.bima.contact.common.dto.BaseListResponseDto;
import com.bima.contact.repositoryservice.dto.ContactDto;
import com.bima.contact.repositoryservice.repository.ContactRepository;
import com.bima.contact.repositoryservice.util.ContactDtoUtil;

@Service
public class ContactRepositoryService {
	
	@Autowired
    private ContactRepository contactRepository;
	
	public BaseListResponseDto<ContactDto> getContactList() {
		return BaseListResponseDto.<ContactDto>builder()
				.responseCode(Constant.SUCCESS_RESPONSE_CODE)
				.responseDesc(Constant.SUCCESS_RESPONSE_DESC)
				.data(ContactDtoUtil.constructContactDtoList(contactRepository.findAll()))
				.build();
	}
	
}
